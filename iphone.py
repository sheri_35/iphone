import os
import time
import csv
import datetime
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from shutil import which
from scrapy.selector import Selector
import json

from selenium.webdriver.chrome.options import Options
from datetime import datetime
from proxy_only import proxy_chrome
import requests
from scrapy.selector import Selector
import csv
import time

ua="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36"


chrome_path=which("chromedriver")
co = Options()
#extension support is not possible in incognito mode for now
#co.add_argument('--incognito')
co.add_argument('--disable-gpu')
co.add_argument('--allow-running-insecure-content')
co.add_argument('--ignore-certificate-errors')
co.add_argument("--enable-javascript")
# co.add_argument("--window-size=1335,712")
co.add_argument("--headless")
# co.add_argument("user-data-dir=selenium")
# co.add_argument("--window-size=1200,"+str(chromesize))
#co.add_argument('--proxy-server=%s' % PROXY)
# co.add_argument('no-sandbox')
#disable infobars
co.add_argument('--disable-infobars')
# co.add_argument("--headless")
co.add_experimental_option("excludeSwitches",["ignore-certificate-errors"])
co.add_argument("--disable-logging")
co.add_argument('--disable-extensions')
co.add_argument('--profile-directory=Default')
# co.add_argument("--incognito")
co.add_argument("--disable-plugins-discovery");
# co.add_extension(path + "\\addon.zip")
co.add_argument("--start-maximized")
co.add_argument('log-level=3')
co.add_argument('--disable-webrtc-encryption')
co.add_argument('--ignore-certificate-errors')
#co.add_argument('headless')
co.add_argument('--disable-gpu')
co.add_argument('--no-sandbox')

# co.add_experimental_option('prefs', {'intl.accept_languages': 'de,de_DE'})
co.add_argument('disable-blink-features=AutomationControlled')
co.add_argument(f'user-agent={ua}')
co.add_argument("--disable-dev-shm-usage")
co.add_argument("--disable-impl-side-painting")
co.add_argument("--disable-setuid-sandbox")
co.add_argument("--disable-seccomp-filter-sandbox")
co.add_argument("--disable-breakpad")
co.add_argument("--disable-client-side-phishing-detection")
co.add_argument("--disable-cast")
co.add_argument("--disable-cast-streaming-hw-encoding")
co.add_argument("--disable-cloud-import")
co.add_argument("--disable-popup-blocking")
co.add_argument("--disable-session-crashed-bubble")
co.add_argument("--disable-ipv6")
co.add_experimental_option("excludeSwitches",["enable-automation"])
# driver=webdriver.Chrome(executable_path=chrome_path,options=co)

session = requests.Session()
c_headers={
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
        'cache-control':'max-age=0',
        'cookie': 'session-id=137-9506669-0869516; session-id-time=2082787201l; i18n-prefs=CAD; ubid-acbca=130-7880828-1726668; lc-acbca=en_CA; session-token=YjYRwooFbjY1quy62bVOLp0zz6BmxFA7wYOM7gUntpxOCumTJRvK0GqrNhj22ArKzJo9uT+M74qdG3WkWdNj47jf79t3HjTcJ/Frag1JOlqpM40JLRApSpDiV3SPEW9OIGtFhKJfNhFa7xxeI/Hlm/O/a0d7chH5yl+VDgsK2FPtJv7fACY2NuU232rVJmLu; csm-hit=tb:EEYW73XEKZS00FY1GV6T+s-YX033ADF1MKAXMX01JZQ|1630591957269&t:1630591957269&adb:adblk_no',
        'downlink':'0.65',
        'ect': '3g',
        'rtt': '500',
        'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
        'sec-ch-ua-mobile': '?0',
        'sec-fetch-dest': 'document',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'none',
        'sec-fetch-user': '?1',
        'service-worker-navigation-preload': 'true',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36'
        }




session.headers=c_headers



with open('URLS.csv','r') as f:
    reader=csv.reader(f)
    for row in reader:
        company=row[0]







        model=row[1]
        memory=row[2]
        colour=row[3]
        grade=row[4]
        url_swappe_nl=row[5]
        url_refurbed_de=row[6]
        url_backmarket_fr=row[7]
        forza_refurbished=row[8]
        refurbished_nl=row[9]
        url_recommerce=row[10]
        url_apple=row[11]


        if company=='Company':
            suggest=[company,model,memory,colour,grade,url_swappe_nl,url_refurbed_de,url_backmarket_fr,forza_refurbished,refurbished_nl,url_recommerce,url_apple]
            print('suggest',suggest)

            try:
                with open('iphone_prices.csv','+a',newline='') as fr:
                    write=csv.writer(fr)
                    write.writerow(suggest)
            except:
                with open('iphone_prices.csv','+a',newline='',encoding='utf-8') as fr:
                    write=csv.writer(fr)
                    write.writerow(suggest)
            continue


        if url_swappe_nl:
            data=session.get(url_swappe_nl)


            response_swappe=Selector(text=data.text)
            url_swappe_nl_price=response_swappe.xpath('//p[@class="sc-qYhTA dNGiKh"]/text()').get()

            # write the script to get that the swapee_data
        else:
            url_swappe_nl_price=''


        if url_refurbed_de:
            data=session.get(url_refurbed_de)


            response_refurbed_de=Selector(text=data.text)


            url_refurbed_de_price_=response_refurbed_de.xpath('//div[@itemprop="offers"]//p[@class="text-2xl text-green-500 font-bold m-0"]/span/text()').get()
            price_2=response_refurbed_de.xpath('//div[@itemprop="offers"]//p[@class="text-2xl text-green-500 font-bold m-0"]/span/following-sibling::text()').get()
            try:
                url_refurbed_de_price=url_refurbed_de_price_+price_2

            except:
                url_refurbed_de_price=url_refurbed_de_price_
        else:
            url_refurbed_de_price=''




        if url_backmarket_fr:
            # data=session.get(url_backmarket_fr)
            driver=webdriver.Chrome(executable_path=chrome_path,options=co)
            driver.get(url_backmarket_fr)
            driver.implicitly_wait(3)
            time.sleep(3)


            response_url_backmarket_fr=Selector(text=driver.page_source)
            driver.close()



            try:
                url_backmarket_fr_price=response_url_backmarket_fr.xpath('//div[@class="flex flex-col md:items-end ml-2 flex-shrink-0"]//h2[@data-qa="productpage-product-price"]/text()').get().strip()
            except:
                url_backmarket_fr_price=response_url_backmarket_fr.xpath('//div[@class="flex flex-col md:items-end ml-2 flex-shrink-0"]//h2[@data-qa="productpage-product-price"]/text()').get()

        else:
            url_backmarket_fr_price=''





        if forza_refurbished:

            data=session.get(forza_refurbished)


            response_forza_refurbished=Selector(text=data.text)



            forza_refurbished_price_=response_forza_refurbished.xpath('//div[@class="product-config-price-"]/span[@class="price"]/text()').getall()
            if grade=='A':
                try:
                    forza_refurbished_price=forza_refurbished_price_[2]
                except:
                    forza_refurbished_price=forza_refurbished_price_[0]

            if grade=='B':
                try:
                    forza_refurbished_price=forza_refurbished_price_[1]
                except:
                    forza_refurbished_price=forza_refurbished_price_[0]

            if grade=='C':
                try:
                    forza_refurbished_price=forza_refurbished_price_[0]
                except:
                    forza_refurbished_price=forza_refurbished_price_[0]


            # forza_refurbished_price=''
            # for frz_p in forza_refurbished_price_:
            #     if frz_p==forza_refurbished_price_[-1]:
            #         forza_refurbished_price+=frz_p
            #     else:
            #         forza_refurbished_price+=frz_p +', '


        else:
            forza_refurbished_price=''



        if refurbished_nl:
            data=session.get(refurbished_nl)


            response_refurbished_nl=Selector(text=data.text)



            try:
                refurbished_nl_price=response_refurbished_nl.xpath('//span[@id="currentProductPrice"]/text()').get().strip()
            except:
                refurbished_nl_price=response_refurbished_nl.xpath('//span[@id="currentProductPrice"]/text()').get()

        else:
            refurbished_nl_price=''




        if url_recommerce:
            data=session.get(url_recommerce)


            response_url_recommerce=Selector(text=data.text)


            url_recommerce_price=response_url_recommerce.xpath('//div[@class="product-shop-top"]//meta[@itemprop="price"]/@content').get()
        else:
            url_recommerce_price=''




        if url_apple:
            data=session.get(url_apple)


            response_url_apple=Selector(text=data.text)



            url_apple_price=response_url_apple.xpath('//div[@class="item equalize-capacity-button-height selection"]//span[@class="price"]/span[@class="current_price"]/text()').get()
        else:
            url_apple_price=''

        suggest=[company,model,memory,colour,grade,url_swappe_nl_price,url_refurbed_de_price,url_backmarket_fr_price,forza_refurbished_price,refurbished_nl_price,url_recommerce_price,url_apple_price]
        print('suggest',suggest)

        try:
            with open('iphone_prices_new.csv','+a',newline='') as fr:
                write=csv.writer(fr)
                write.writerow(suggest)
        except:
            with open('iphone_prices_new.csv','+a',newline='',encoding='utf-8') as fr:
                write=csv.writer(fr)
                write.writerow(suggest)
